Version 1/181209 of Artificial brain by Allison Edwards begins here.
"An extension focused on writing more sophisticated NPC actions."



Chapter 0 - Merely some more creditable people

Academic credits for artifical brain are always "Inspired by this and other paper and thesis'

http://publications.lib.chalmers.se/records/fulltext/245348/245348.pdf
by
Gustav Grund Pihlgren
Martin Nilsson
Mikael Larsson
Oskar Olsson
Tobias Foughman
Victor Gustafsson".


Chapter 1 - NPC AI and State Machine Framework

[NPC  state machines 

	Each state is a rulebook based on and producing a rulebook.
	Each person has a current state.
	Each person has a list of intentions (todos).
	Each State can 'push' a state from an independent state machine or add to the
	  npc todo list.
	Game World created new actions add to the todo list.
	  the todo list is the initial state of a state machine for a new task for that npc.
	
	The Idle State:
		When in the idle state we pop a task from the todo list if possible. Else we remain
		in the idle state.
		
	Turn count stack.
		If we don't change state we increment a turns in this state counter .
		We can use this to implement 'bored' eg, if idle state and turn count > x
		
	-- I might need to add
	State argument stack.
		A Stack of arguments used for each state - this needs a little thought as
		control of push/pop might be more subtle. 
		arguments are a list of things.
		
		
	The phrases,
	   set a person state to state
	   instruct person todo state,
	   instruct person todo state with arguments xx
	   remove a todo state [nyi]
	   ( rulebook) persons next todo
	   pop state from person,
	   increment persons turn count
	   ( number ) turns person in current state.
	   ( rulebook) persons current state
	   persons's current state arguments
	   ( number ) person outstanding todos	 [nyi]
	   ( rulebook  ) persons ( number ) st/nd//rd/th/-- outstanding todo [nyi]
	   starting doing todo nr  x [nyi]
	
	
	
	Form the public API to the person data management, only these should access a person's
	properties directly.
	
	This is so better implementations can be used later and we can keep compatibility.
	
]


A person has a list of ( person based rulebooks ) called state stack . a person has a list of numbers called turn count stack.
A person has a list of ( person based rulebooks ) called todo.

a person has a list of ( list of objects ) called the argument stack. 
a person has a list of ( list of objects ) called the todo argument list. 


[ Define some convenience phrases for dealing with state stacks]

[ Change the state, this is seperate from pushing, and leaves the argument stack alone ]
to set (npc - a person) state to (a new state - person based rulebook):
	let N be the number of entries in state stack of the npc;
	now entry N of state stack of the npc  is a new state;
	now entry N of the turn count stack of the npc is 0;

	
			
to instruct (npc -  a person) to do (a new task - person based rulebook):
	instruct the npc to do a new task with { }.



to decide which people based rulebook is (npc - a persons) next todo:
	let td be entry 0 of the todo of npc;
	decide on td.
	

			
to instruct (npc -  a person) to do (a new task - person based rulebook) with (arguments - list of objects):
	add a new task to the todo of the npc;
	add arguments to the todo argument list of the npc.
 
to push state (a new state - a person based rulebook) onto (npc - a person) with (arguments - list of objects):
	add a new state to the state stack of the npc;
	add 0 to the turn count stack of the npc;
	add arguments to the argument stack of the npc.
		
to push state (a new state - a person based rulebook) onto (npc - a person) :
	push state a new state onto the npc with { }.

to pop a state from (npc - a person):
	let N be the number of entries in the state stack of the npc;
	[ Popping an empty stack shall be a noop ]
	if N is greater than 0:
		remove entry N from the state stack of the npc;
		remove entry N from the turn count stack of the npc;
		remove entry N from the argument stack of the npc.
	

to pop a state from (npc - a person) and continue:
	pop a state from npc;
	let N be the number of entries in the state stack of the npc;
	if N is greater than 0:
		let the current state be entry N of the state stack of the npc;
		follow the current state for the npc;
	else:
		follow idle for the Npc.
	
to increment (npc - a persons) current turn count:
	let N be the number of entries in the state stack of the npc;
	let V be entry N of the turn count stack of the npc plus one;
	now entry N of the turn count stack of the npc is V;


to decide which number is turns (npc - a person) has spent in the current state:
	let N be the number of entries in the state stack of the npc;
	let V be entry N of the turn count stack of the npc;
	decide on V.
	

to decide which people based rulebook is (npc - a persons) current state:
	let N be the number of entries in the state stack of the npc;
	let V be entry N of state stack of the npc;
	decide on V.

	
to decide which list of objects is  (npc - a persons) current state arguments:
	let N be the number of entries in the argument stack of the npc;
	let V be entry N of argument stack of the npc;
	decide on V.



to push a new task on (npc - a persons) stack:
	let task be entry 1 of the todo of the npc;
 	let args be entry 1 of the todo argument list of the npc;
	remove entry 1 from the todo of the npc  ;
 	remove entry 1 from the todo argument list of the npc;
	push state task onto  the npc with args.
	
	
	
to (npc - a person ) starts their next todo:
	npc starts their todo numbered 1.
	
	
to (npc - a person ) starts their todo numbered ( N - a number):
 	let state be entry N in the todo of the npc;
	let args be entry N in the todo argument list of the npc;	
	push state state onto npc with args.



to decide which people based rulebook is the current state of the  (npc - a person):
	let N be the number of entries in the state stack of the npc;
	let V be entry N of the state stack of the npc;
	decide on V.
	



[
The idle rulebook provides rules when there is no other action,
 it in general pop actions off the todo list and push them on the
 current stack. - We declare it here ; add the every turn which erefence 
then define it for general case below,.
]

Idle is a  person based rulebook.


[ process the npc actions ]
every turn:
	repeat with npc running through persons:
		let the stack be the state stack of the npc;
		let N be the number of entries in the stack;
		if N is greater than zero:
			let X be entry N of the turn count stack of the npc;
			let the current state be entry N of the state stack of the npc;
			[info "STM [npc] [n] [X] [line break]";]
			[info "[current state][line break]";]
			follow the current state for the npc;
			increment the npc current turn count;
		else:
			push state Idle onto the the npc.



An idle person (called the npc):
	let the busyness be the number of entries in the state stack of the npc;
	[Check if there are states behind Idle on the stack]
	if the busyness is greater than one:
		[ so if now idle; but was doing something - return to previous task ]
		pop a state from the Npc and continue;
	else:
		let needed roundtoits be the number of entries in todo of the npc;
		if needed roundtoits is greater than zero:
			let nextjob be entry 0 in todo of the npc;
			remove entry 0 from todo of the npc;
			push state nextjob onto the npc.[;
		else:
			info "[npc] is bored now.".]


[Idle a diligent worker (called W): [...]
Idle a lazy worker (called W): say "[w] does nothing" instead;
]

when play begins:
	repeat with npc running through people who are not the player:
		[if the npc is self:]
		push state Idle onto the npc;



[

NPC state machine tests are a test suite.
NPC state machine is a test case.

to test the NPC state machine:
]	

Chapter Interface to threaded conversation (for use with Threaded Conversation by Chris Conley)

A quip has a person based rulebook called the interlocutor next state.
The interlocutor next state of a quip is usually idle. 
A quip has a list of objects called the interlocutor next state arguments .

after  someone discussing something (This is the conversation affects a person actions rule):
	push state interlocutor next state of the noun onto the current interlocutor with the interlocutor next state arguments of the noun;
	continue the action.


Artificial brain ends here.
